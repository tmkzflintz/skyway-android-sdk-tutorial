package com.ntt.ecl.webrtc.sample_p2p_videochat;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;

//
import io.skyway.Peer.Browser.Canvas;
import io.skyway.Peer.Browser.MediaConstraints;
import io.skyway.Peer.Browser.MediaStream;
import io.skyway.Peer.Browser.Navigator;
import io.skyway.Peer.CallOption;
import io.skyway.Peer.MediaConnection;
import io.skyway.Peer.OnCallback;
import io.skyway.Peer.Peer;
import io.skyway.Peer.PeerError;
import io.skyway.Peer.PeerOption;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
//

public class MainActivity extends Activity {
	private static final String TAG = MainActivity.class.getSimpleName();

	//
	// Set your APIkey and Domain
	//
	private static final String API_KEY = "cfbdc104-5853-465b-84df-b38a202f09a7";
	private static final String DOMAIN = "locksi.cloud";




	//
	// declaration
	//
	private Peer			_peer;
	private MediaStream		_localStream;
	private MediaStream		_remoteStream;
	private MediaConnection	_mediaConnection;

	private String			_strOwnId;
	private boolean			_bConnected;

	private Handler			_handler;

	private JsonPlaceHolderApi jsonPlaceHolderApi;

	private Station station;
	private int deviceId;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		//
		// Windows title hidden
		//
		Window wnd = getWindow();
		wnd.addFlags(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		//
		// Set UI handler
		//
		_handler = new Handler(Looper.getMainLooper());
		final Activity activity = this;

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl("https://locksi.cloud/")
				.addConverterFactory(GsonConverterFactory.create())
				.build();

		this.jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

		//
		// Initialize Peer
		//
		PeerOption option = new PeerOption();
		option.key = API_KEY;
		option.domain = DOMAIN;
		option.debug = Peer.DebugLevelEnum.ALL_LOGS;
		_peer = new Peer(this, option);

		//
		// Set Peer event callbacks
		//

		// OPEN
		_peer.on(Peer.PeerEventEnum.OPEN, new OnCallback() {
			@Override
			public void onCallback(Object object) {

				// Show my ID
				_strOwnId = (String) object;
				TextView tvOwnId = (TextView) findViewById(R.id.tvOwnId);
				tvOwnId.setText(_strOwnId);
				deviceId = 4;
				updateStaion(_strOwnId, deviceId);

			}
		});

		if (ContextCompat.checkSelfPermission(activity,
				Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(activity,
				Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},0);
			Log.d(TAG, "request permission");
		}
		else {

			// Get a local MediaStream & show it
			startLocalStream();
			Log.d(TAG, "open camera");
		}

		// ERROR
		_peer.on(Peer.PeerEventEnum.ERROR, new OnCallback() {
			@Override
			public void onCallback(Object object) {
				PeerError error = (PeerError) object;
				Log.d(TAG, "[On/Error]" + error);
//				logTxt.setText("[On/Error]" + error);
			}
		});

		// CLOSE
		_peer.on(Peer.PeerEventEnum.CLOSE, new OnCallback()	{
			@Override
			public void onCallback(Object object) {
				Log.d(TAG, "[On/Close]");
//				logTxt.setText("[On/Close]");
			}
		});

		// DISCONNECTED
		_peer.on(Peer.PeerEventEnum.DISCONNECTED, new OnCallback() {
			@Override
			public void onCallback(Object object) {
				Log.d(TAG, "[On/Disconnected]");
//				logTxt.setText("[On/Disconnected]");
			}
		});

		// CALL (Incoming call)
		_peer.on(Peer.PeerEventEnum.CALL, new OnCallback() {
			@Override
			public void onCallback(Object object) {
				if (!(object instanceof MediaConnection)) {
					return;
				}

				_mediaConnection = (MediaConnection) object;
				setMediaCallbacks();
				_mediaConnection.answer(_localStream);

				_bConnected = true;
				updateActionButtonTitle();
			}
		});

		//
		// Set GUI event listeners
		//

		// Set GUI event listner for Button (make/hang up a call)
		Button btnAction = (Button) findViewById(R.id.btnAction);
		btnAction.setEnabled(true);
		btnAction.setOnClickListener(new View.OnClickListener()	{
			@Override
			public void onClick(View v)	{
				v.setEnabled(false);

				if (!_bConnected) {

					// Select remote peer & make a call

					getStation();
				}
				else {

					station = null;
					closeRemoteStream();
					_mediaConnection.close();
				}

				v.setEnabled(true);
			}
		});

		// Action for switchCameraButton
//		Button switchCameraAction = (Button)findViewById(R.id.switchCameraAction);
//		switchCameraAction.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v)	{
//				if(null != _localStream){
//					Boolean result = _localStream.switchCamera();
//					if(true == result)	{
//						//Success
//					}
//					else {
//						//Failed
//					}
//				}
//
//			}
//		});

	}

	public void getStation() {
		Call<Station> call = jsonPlaceHolderApi.getStation(1);

		call.enqueue(new Callback<Station>() {
			@Override
			public void onResponse(Call<Station> call, Response<Station> response) {

				if (!response.isSuccessful()) {
//					logTxt.setText("Code: " + response.code());
					return;
				}
				station = new Station();
				station = response.body();
				onRequestCall(station.call_id);

				String content = "";
				content += "ID: " + station.id + "\n";
				content += "Call_id: " + station.call_id + "\n\n";

//				logTxt.setText(content);
			}

			@Override
			public void onFailure(Call<Station> call, Throwable t) {
//				logTxt.setText(t.getMessage());
			}
		});
	}

	private void updateStaion(String call_id, int id) {
		Station upStation = new Station();
		upStation.id = id;
		upStation.call_id = call_id;

		Call<Station> call = jsonPlaceHolderApi.updateStation(upStation);

		call.enqueue(new Callback<Station>() {
			@Override
			public void onResponse(Call<Station> call, Response<Station> response) {

				if (!response.isSuccessful()) {
//					logTxt.setText("Code: " + response.code());
					return;
				}
				station = response.body();
//				logTxt.setText("call id is Updated.");
			}

			@Override
			public void onFailure(Call<Station> call, Throwable t) {
//				logTxt.setText(t.getMessage());
			}
		});
	}

	//
	// onRequestPermissionResult
	//
	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
			case 0: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					startLocalStream();
				}
				else {
					Toast.makeText(this,"Failed to access the camera and microphone.\nclick allow when asked for permission.", Toast.LENGTH_LONG).show();
				}
				break;
			}
		}
	}

	//
	// Activity Lifecycle
	//
	@Override
	protected void onStart() {
		super.onStart();

		Window wnd = getWindow();
		wnd.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		wnd.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	protected void onResume() {
		super.onResume();

		setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
	}

	@Override
	protected void onPause() {

		setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
		super.onPause();
	}

	@Override
	protected void onStop()	{
		Window wnd = getWindow();
		wnd.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		wnd.clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		destroyPeer();
		super.onDestroy();
	}

	//
	// Get a local MediaStream & show it
	//
	void startLocalStream() {
		MediaConstraints constraints = new MediaConstraints();
		constraints.maxWidth = 960;
		constraints.maxHeight = 540;
		constraints.cameraPosition = MediaConstraints.CameraPositionEnum.FRONT;

		// continued below ...
		Navigator.initialize(_peer);
		_localStream = Navigator.getUserMedia(constraints);
		Canvas canvas = (Canvas) findViewById(R.id.svLocalView);
		_localStream.addVideoRenderer(canvas,0);
	}

	//
	// Set callbacks for MediaConnection.MediaEvents
	//
	void setMediaCallbacks() {

		_mediaConnection.on(MediaConnection.MediaEventEnum.STREAM, new OnCallback() {
			@Override
			public void onCallback(Object object) {
				_remoteStream = (MediaStream) object;
				Canvas canvas = (Canvas) findViewById(R.id.svRemoteView);
				_remoteStream.addVideoRenderer(canvas,0);
			}
		});

		_mediaConnection.on(MediaConnection.MediaEventEnum.CLOSE, new OnCallback()	{
			@Override
			public void onCallback(Object object) {
				closeRemoteStream();
				_bConnected = false;
				updateActionButtonTitle();
			}
		});

		_mediaConnection.on(MediaConnection.MediaEventEnum.ERROR, new OnCallback()	{
			@Override
			public void onCallback(Object object) {
				PeerError error = (PeerError) object;
				Log.d(TAG, "[On/MediaError]" + error);
			}
		});

		// continues below ...
	}



	//
	// Clean up objects
	//
	private void destroyPeer() {
		closeRemoteStream();

		if (null != _localStream) {
			Canvas canvas = (Canvas) findViewById(R.id.svLocalView);
			_localStream.removeVideoRenderer(canvas,0);
			_localStream.close();
		}

		if (null != _mediaConnection)	{
			if (_mediaConnection.isOpen()) {
				_mediaConnection.close();
			}
			unsetMediaCallbacks();
		}

		Navigator.terminate();

		if (null != _peer) {
			unsetPeerCallback(_peer);
			if (!_peer.isDisconnected()) {
				_peer.disconnect();
			}

			if (!_peer.isDestroyed()) {
				_peer.destroy();
			}

			_peer = null;
		}
	}

	//
	// Unset callbacks for PeerEvents
	//
	void unsetPeerCallback(Peer peer) {
		if(null == _peer){
			return;
		}

		peer.on(Peer.PeerEventEnum.OPEN, null);
		peer.on(Peer.PeerEventEnum.CONNECTION, null);
		peer.on(Peer.PeerEventEnum.CALL, null);
		peer.on(Peer.PeerEventEnum.CLOSE, null);
		peer.on(Peer.PeerEventEnum.DISCONNECTED, null);
		peer.on(Peer.PeerEventEnum.ERROR, null);
	}

	//
	// Unset callbacks for MediaConnection.MediaEvents
	//
	void unsetMediaCallbacks() {
		if(null == _mediaConnection){
			return;
		}

		_mediaConnection.on(MediaConnection.MediaEventEnum.STREAM, null);
		_mediaConnection.on(MediaConnection.MediaEventEnum.CLOSE, null);
		_mediaConnection.on(MediaConnection.MediaEventEnum.ERROR, null);
	}

	//
	// Close a remote MediaStream
	//
	void closeRemoteStream(){
		if (null == _remoteStream) {
			return;
		}

		Canvas canvas = (Canvas) findViewById(R.id.svRemoteView);
		_remoteStream.removeVideoRenderer(canvas,0);
		_remoteStream.close();
	}


	//
	// Create a MediaConnection
	//
	void onRequestCall(String strPeerId) {
		if (null == _peer) {
			return;
		}

		if (null != _mediaConnection) {
			_mediaConnection.close();
		}

		CallOption option = new CallOption();
		_mediaConnection = _peer.call(strPeerId, _localStream, option);

		if (null != _mediaConnection) {
			setMediaCallbacks();
			_bConnected = true;
		}

		updateActionButtonTitle();
	}

	//
	// Listing all peers
	//
//	void showPeerIDs() {
//		if ((null == _peer) || (null == _strOwnId) || (0 == _strOwnId.length())) {
//			Toast.makeText(this, "Your PeerID is null or invalid.", Toast.LENGTH_SHORT).show();
//			return;
//		}
//
//		// Get all IDs connected to the server
//		final Context fContext = this;
//		_peer.listAllPeers(new OnCallback() {
//			@Override
//			public void onCallback(Object object) {
//				if (!(object instanceof JSONArray)) {
//					return;
//				}
//
//				JSONArray peers = (JSONArray) object;
//				ArrayList<String> _listPeerIds = new ArrayList<>();
//				String peerId;
//
//				// Exclude my own ID
//				for (int i = 0; peers.length() > i; i++) {
//					try {
//						peerId = peers.getString(i);
//						if (!_strOwnId.equals(peerId)) {
//							_listPeerIds.add(peerId);
//						}
//					} catch(Exception e){
//						e.printStackTrace();
//					}
//				}
//
//				// Show IDs using DialogFragment
//				if (0 < _listPeerIds.size()) {
//					FragmentManager mgr = getFragmentManager();
//					PeerListDialogFragment dialog = new PeerListDialogFragment();
//					dialog.setListener(
//							new PeerListDialogFragment.PeerListDialogFragmentListener() {
//								@Override
//								public void onItemClick(final String item) {
//									_handler.post(new Runnable() {
//										@Override
//										public void run() {
//											onRequestCall(item);
//										}
//									});
//								}
//							});
//					dialog.setItems(_listPeerIds);
//					dialog.show(mgr, "peerlist");
//				}
//				else{
//					Toast.makeText(fContext, "PeerID list (other than your ID) is empty.", Toast.LENGTH_SHORT).show();
//				}
//			}
//		});
//
//	}


	//
	// Update actionButton title
	//
	void updateActionButtonTitle() {
		_handler.post(new Runnable() {
			@Override
			public void run() {
				Button btnAction = (Button) findViewById(R.id.btnAction);
				if (null != btnAction) {
					if (false == _bConnected) {
						btnAction.setText("Call");
					} else {
						btnAction.setText("End Call");
					}
				}
			}
		});
	}
	
}

