package com.ntt.ecl.webrtc.sample_p2p_videochat;

import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StationController {
    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private TextView textView;
    public StationController(TextView textView){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://locksi.cloud/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        this.textView = textView;
    }
    public void getStation() {
        Call<Station> call = jsonPlaceHolderApi.getStation(1);

        call.enqueue(new Callback<Station>() {
            @Override
            public void onResponse(Call<Station> call, Response<Station> response) {

                if (!response.isSuccessful()) {
                    textView.setText("Code: " + response.code());

                }

                Station postResponse = response.body();

//				String content = "";
//				content += "Code: " + response.code() + "\n";
//				content += "ID: " + postResponse.getId() + "\n";
//				content += "User ID: " + postResponse.getUserId() + "\n";
//				content += "Title: " + postResponse.getTitle() + "\n";
//				content += "Text: " + postResponse.getText() + "\n\n";

                textView.setText(postResponse.toString());
            }

            @Override
            public void onFailure(Call<Station> call, Throwable t) {
                textView.setText(t.getMessage());
            }
        });
    }

    private void updateStaion(String call_id) {
        Station station = new Station();
        station.call_id = call_id;

        Call<Station> call = jsonPlaceHolderApi.updateStation(station);

        call.enqueue(new Callback<Station>() {
            @Override
            public void onResponse(Call<Station> call, Response<Station> response) {

                if (!response.isSuccessful()) {
                    textView.setText("Code: " + response.code());
                    return;
                }

                Station postResponse = response.body();

//				String content = "";
//				content += "Code: " + response.code() + "\n";
//				content += "ID: " + postResponse.getId() + "\n";
//				content += "User ID: " + postResponse.getUserId() + "\n";
//				content += "Title: " + postResponse.getTitle() + "\n";
//				content += "Text: " + postResponse.getText() + "\n\n";

                textView.setText(postResponse.toString());
            }

            @Override
            public void onFailure(Call<Station> call, Throwable t) {
                textView.setText(t.getMessage());
            }
        });
    }
}
